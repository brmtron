package cz.brmlab.brmtron.server.test;

import static org.junit.Assert.*;

import org.junit.Test;

import cz.brmlab.brmtron.server.Player;
import cz.brmlab.brmtron.server.ProtocolHelper;

public class ProtocolHelperTest {

	@Test
	public void test_parsePlayer(){
		Player p;
		String input = "1 --- tester --- 1.1123 --- 54.4234 --- true\n";
		
		p = ProtocolHelper.parsePlayer(input);
		
		assertTrue(p.getId() == 1);
		assertTrue(p.getName().equals("tester"));
		assertTrue(p.getLongitude() == 1.1123);
		assertTrue(p.getLatitude() == 54.4234);
		assertTrue(p.isActive());
	}
	
	@Test
	public void test_buildPlayer() {
		String inp = "1 --- tester --- 1.1123 --- 54.4234 --- true\n";
		Player p = ProtocolHelper.parsePlayer(inp);
		String s = ProtocolHelper.buildPlayer(p);
		assertTrue(s.equals(inp));
	}
}
