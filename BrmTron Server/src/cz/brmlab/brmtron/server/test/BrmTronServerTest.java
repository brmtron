package cz.brmlab.brmtron.server.test;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import cz.brmlab.brmtron.server.BrmTronServer;
import cz.brmlab.brmtron.server.PathElement;

public class BrmTronServerTest extends BrmTronServer{
	
	@Test
	public void test_calcIntersection() {
		final double dx = 1e-09;
		
		double[] start1 = {0.0, 0.0};
		double[] end1 = {0.0, 2.0};
		double[] start2 = {-1.0, 0.0};
		double[] end2 = {1.0, 2.0};
		
		double[] res = {0.0, 1.0};
		
		PathElement e1 = new PathElement(start1, end1);
		PathElement e2 = new PathElement(start2, end2);
		
		double[] brm = BrmTronServer.calcIntersection(e1, e2);
		
		assertTrue(compareDouble(brm, res, dx));
	}
	
	@Test
	public void test1_findIntersection() {
		final double dx = 1e-09;
		
		double[] start1 = {0.0, 0.0};
		double[] end1 = {0.0, 2.0};
		double[] start2 = {-1.0, 0.0};
		double[] end2 = {1.0, 2.0};
		
		double[] res = {0.0, 1.0};
		
		PathElement e1 = new PathElement(start1, end1);
		PathElement e2 = new PathElement(start2, end2);
		
		double[] brm = BrmTronServer.findIntersection(e1, e2);
		
		assertTrue(compareDouble(brm, res, dx) );
	}
	
	@Test
	public void test2_findIntersection() {
		final double dx = 1e-09;
		
		double[] start1 = {0.1, 0};
		double[] end1 = {0.0, 2.0};
		double[] start2 = {-1.0, 0};
		double[] end2 = {1.0, 200.0};
		
		double[] res = null;
		
		PathElement e1 = new PathElement(start1, end1);
		PathElement e2 = new PathElement(start2, end2);
		
		double[] brm = BrmTronServer.findIntersection(e1, e2);
		
		assertTrue(compareDouble(brm, res, dx));
	}
	
	@Test
	public void test1_checkForCollision() {
		generateWalls(500);
		double[] start = {0,1};
		double [] end = {1,0};
		PathElement p = new PathElement(start, end);
		
		assertFalse(checkForCollision(p) == null);		
		
		p.setStart(500, 893);
		p.setEnd(543, 489.1);
		
		assertTrue(checkForCollision(p) == null);
	}
	
	private boolean compareDouble(double[] a, double[] b, double dx) {
		if(a == null && b == null) 
			return true;
		if((a == null && b != null) || (a != null && b ==null))
			return false;
		boolean ret=true;
		for(int i=0; i<a.length; i++) {
			if(Math.abs(a[i]-b[i]) > dx) {
				ret = false;
				break;
			}
			
		}
		return ret;
	}
	
	private void generateWalls(int n) {
		Random r = new Random();
		double[] start = {0,0};
		double[] end = {0,0};
		while((n--)>0) {
			start = end.clone();
			end[0] = start[0] + r.nextDouble();
			end[1] = start[1] + r.nextDouble();
			PathElement p = new PathElement(start, end);
			addWall(p);
		}
	}

}
