package cz.brmlab.brmtron.server;

import java.util.Map;
import java.util.Set;

public class ProtocolHelper {

	//id --- name --- longitude --- latitude --- active\n
	
	private static final int FIELD_ID = 0;
	private static final int FIELD_NAME = 1;
	private static final int FIELD_LONGITUDE = 2;
	private static final int FIELD_LATITUDE = 3;
	private static final int FIELD_ACTIVE = 4;
	private static final String FIELD_DELIMITER = " --- ";
	
	public static Player parsePlayer(String input) {
		//TODO overeni vstupu
		String[] tmp = input.trim().split(FIELD_DELIMITER);
		int id = Integer.parseInt(tmp[FIELD_ID]);
		Player ret = new Player(id);
		ret.setName(tmp[FIELD_NAME]);
		ret.setCoords(Double.parseDouble(tmp[FIELD_LONGITUDE]), Double.parseDouble(tmp[FIELD_LATITUDE]));
		ret.setActive(Boolean.parseBoolean(tmp[FIELD_ACTIVE]));
		return ret;
	}
	
	public static String buildPlayer(Player p) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(p.getId());
		sb.append(FIELD_DELIMITER);
		sb.append(p.getName());
		sb.append(FIELD_DELIMITER);
		sb.append(p.getLongitude());
		sb.append(FIELD_DELIMITER);
		sb.append(p.getLatitude());
		sb.append(FIELD_DELIMITER);
		sb.append(p.isActive());
		sb.append("\n");
		
		return sb.toString();
	}
	
	
	/**
	 * @param players Numbered list of players
	 * @return String representation of each player's state
	 */
	public static String getUpdate(Map<Integer, Player> players) {
		StringBuilder sb = new StringBuilder();
		Set<Integer> keys = players.keySet();
		
		for(int i: keys) {
			sb.append(buildPlayer(players.get(i)));
		}
		
		return sb.toString();
	}
}
