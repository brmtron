package cz.brmlab.brmtron.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class BrmTronServerThread extends Thread {

	private Socket sock;
	private int id;
	private Player player;
	
	
	public BrmTronServerThread(Socket sock, int id){
		super("BrmTronServerThread");
		this.sock = sock;
		this.id = id;
		this.player = new Player(id);
	}
	
	@Override
	public void run() {
		String s;

		try {
			DataOutputStream out = new DataOutputStream(sock.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			
			player.setActive(true);
			
			while((s=in.readLine()) != null) {
				//debug commands
				if(s.equalsIgnoreCase("die")){
					player.setActive(false);
				}
				if(s.equals("quit")) {
					break;
				}
				
				//parse client input
				Player np = ProtocolHelper.parsePlayer(s);
				player.merge(np);
				
				PathElement elem = new PathElement(player.getPrevCoords(), player.getCoords());
				
				//collision check
				double[] tmp = BrmTronServer.checkForCollision(elem);

				//add a wall; if player has crashed, change his end position to a crash location 
				if(tmp == null) {
					BrmTronServer.addWall(elem);
				} else {
					elem.setEnd(tmp[0], tmp[1]);
					BrmTronServer.addWall(elem);
					player.setActive(false);
					player.setCoords(tmp[0], tmp[1]);
				}
				
				//save player state and send him an update
				BrmTronServer.updatePlayer(id, player);
				out.writeBytes(ProtocolHelper.getUpdate(BrmTronServer.getPlayers()));
				
			}
			out.close();
			in.close();
			sock.close();
		} catch(Exception e) {
			System.err.println(e);
		}

	}


}
