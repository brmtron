package cz.brmlab.brmtron.server;

public class Player {
	
	private int id=0;
	private String name = "Player"; 
	private double longitude = 0;
	private double latitude = 0;
	private double[] prevCoords = new double[2];
	private boolean active = false;
	
	public Player(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public double getLongitude() {
		return longitude;
	}
	
	public double[] getCoords(){
		double[] ret = new double[2];
		ret[0] = getLongitude();
		ret[1] = getLatitude();
		return ret;
	}
	
	public void setCoords(double x, double y) {
		this.setPrevCoords(longitude, latitude);
		this.longitude = x;
		this.latitude = y;
	}
	
	public void setPrevCoords(double x, double y) {
		this.prevCoords[0] = x;
		this.prevCoords[1] = y;
	}
	
	public double[] getPrevCoords() {
		return prevCoords;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void merge(Player np) {
		//TODO lip osetrit merge Playeru
		longitude = np.getLongitude();
		latitude = np.getLatitude();
	}

}
