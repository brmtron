package cz.brmlab.brmtron.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BrmTronServer {

	private static final int PORT = 4567;
	
	static Socket s;
	private static boolean active = true;
	private static int threadId = 1;
	
	private static List<PathElement> walls = new LinkedList<PathElement>();
	
	private static Map<Integer, Player> players = new LinkedHashMap<Integer, Player>();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ServerSocket srvSock = null;
		
		try {
			srvSock = new ServerSocket(PORT);
		} catch(Exception e) {
			System.err.println(e);
			System.exit(-1);
		}
		
		try {
			while(active) {
				new BrmTronServerThread(srvSock.accept(), threadId++).start();
			}
		} catch(Exception e) {
			System.err.println(e);
		}
	}
	
	public static void updatePlayer(int id, Player p) {
		//FIXME nemusime dostavat kompletni informace, chce to nejak mergovat
		players.put(id, p);
	}
	
	public static Map<Integer, Player> getPlayers() {
		return players;
	}
	
	/**
	 * @param wall Wall to add
	 * Adds new wall to the list of the walls.
	 */
	public static void addWall(PathElement wall) {
		walls.add(wall);
	}
	
	/**
	 * @param p1
	 * @return Coordinates of collision, if exists, otherwise returns null.
	 * Checks given line against internal list of walls for collision.
	 */
	public static double[] checkForCollision(PathElement p1) {
		//threshold for near-only optimization
		final double threshold = 5;
		
		double[] collisionCoords = null;
		List<PathElement> brm = new LinkedList<PathElement>();
		
		for(PathElement p : walls) {
			if(isNear(p1, p, threshold))
				brm.add(p);
		}
		
		for(PathElement p2 : brm) {
			collisionCoords = findIntersection(p1,p2);
			if(collisionCoords != null) 
				break;
		}
		return collisionCoords;
	}
	
	/**
	 * @param p1 First line to check
	 * @param p2 Second line to check
	 * @param threshold
	 * @return
	 * Determines, if two lines are too far from each other or not. 
	 * If start point of line 2 belongs to a square area determined by start point of line 1 and given threshold.     
	 */
	private static boolean isNear(PathElement p1, PathElement p2, double threshold) {
		if(Math.abs(p2.getStart()[0] - p1.getStart()[0]) > threshold || 
				Math.abs(p2.getStart()[1] - p1.getStart()[1]) > threshold) {
			return false;
		} else 
			return true;
	}
	
	/**
	 * @param line1
	 * @param line2
	 * @return Coordinates of intersection, if exists, otherwise returns null.
	 * Finds an intersection of two lines.
	 */
	public static double[] findIntersection(PathElement line1, PathElement line2) {
		double[] intersection = null;
		double[] start1 = line1.getStart();
		double[] end1 = line1.getEnd();
		double[] start2 = line2.getStart();
		double[] end2 = line2.getEnd();
		
		double x1min = Math.min(start1[0], end1[0]);
		double x2min = Math.min(start2[0], end2[0]);
		double x1max = Math.max(start1[0], end1[0]);
		double x2max = Math.max(start2[0], end2[0]);
		double y1min = Math.min(start1[1], end1[1]);
		double y2min = Math.min(start2[1], end2[1]);
		double y1max = Math.max(start1[1], end1[1]);
		double y2max = Math.max(start2[1], end2[1]);
		
		if((y2min > y1max) || (y2max < y1min) || (x2max < x1min) || (x2min > x1max))
			return null;
		
		if(checkLines(y1min, y2min, y1max, y2max, start1, start2, end1, end2) ||
				checkLines(y2min, y1min, y2max, y1max, start2, start1, end2, end1)) 
			return calcIntersection(line1, line2);
		
		return intersection;
	}
	
	/**
	 * @param y1min
	 * @param y2min
	 * @param y1max
	 * @param y2max
	 * @param start1
	 * @param start2
	 * @param end1
	 * @param end2
	 * @return 
	 * This method determines, if given lines cross each other or not.
	 */
	protected static boolean checkLines(double y1min, double y2min, double y1max, double y2max, double[] start1, double[] start2, double[] end1, double[] end2) {
		//FIXME rozbiji se to, pokud jsou nejake souradnice stejne (v praxi se to sice skoro nestane, ale stejne je to fuj)
		if(y1min <= y2min) {
			if(y1max < y2min)
				return false;
			
			double d1,d2;
			double a,b;
			
			if(y1min == start1[1]) {	//tady se to rozbiji, chce to tu identifikaci bodu vyresit jinak; kdyz jsou y-souradnice stejny, tak je rozbijou rovnice dole
				a = start1[0];
				b = end1[0];
			} else {
				a = end1[0];
				b = start1[0];
			}
			
			d1 = Math.signum(a - (start2[0]+((end2[0] - start2[0])*(y1min-start2[1]))/(end2[1] - start2[1])));
			d2 = Math.signum(b - (start2[0]+((end2[0] - start2[0])*(y1max-start2[1]))/(end2[1] - start2[1])));
			
			if(d1 == -d2)
				return true;
		}
		return false;
	}
	
	/**
	 * @param line1
	 * @param line2
	 * @return Coordinates of intersection.
	 * Finds coordinates of intersection of given lines. This method expects that the intersection exists.
	 */
	protected static double[] calcIntersection(PathElement line1, PathElement line2){
		double a1, b1, c1, a2, b2, c2, det_inv, m1, m2;
		double[] ret = new double[2];
		
		double x0 = line1.getStart()[0];
		double y0 = line1.getStart()[1];
		double x1 = line1.getEnd()[0];
		double y1 = line1.getEnd()[1];
		double x2 = line2.getStart()[0];
		double y2 = line2.getStart()[1];
		double x3 = line2.getEnd()[0];
		double y3 = line2.getEnd()[1];
		
		if ((x1-x0)!=0)
			m1 = (y1-y0)/(x1-x0);
		else
			m1 = (double)1e+10;  
		
		if ((x3-x2)!=0)
			m2 = (y3-y2)/(x3-x2);
		else
			m2 = (double)1e+10;   

		a1 = m1;
		a2 = m2;

		b1 = -1;
		b2 = -1;

		c1 = (y0-m1*x0);
		c2 = (y2-m2*x2);

		det_inv = 1/(a1*b2 - a2*b1);
		ret[0]=((b1*c2 - b2*c1)*det_inv);
		ret[1]=((a2*c1 - a1*c2)*det_inv);
		
		return ret;
	}

}
