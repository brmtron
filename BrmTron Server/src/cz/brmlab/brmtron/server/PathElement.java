package cz.brmlab.brmtron.server;

public class PathElement {
	private double startX;
	private double startY;
	private double endX;
	private double endY;
	
	public PathElement(double[] start, double[] end) {
		this.startX = start[0];
		this.startY = start[1];
		this.endX = end[0];
		this.endY = end[1];
	}
	
	public double[] getStart() {
		double[] start = new double[2];
		start[0] = startX;
		start[1] = startY;
		return start;
	}
	
	public double[] getEnd() {
		double[] end = new double[2];
		end[0] = endX;
		end[1] = endY;
		return end;
	}
	
	public void setStart(double x, double y) {
		this.startX = x;
		this.startY = y;
	}
	
	public void setEnd(double x, double y) {
		this.endX = x;
		this.endY = y;
	}
}
