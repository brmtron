package cz.brmlab.brmtron.client;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditOptions extends Activity {

	SharedPreferences prefs;
	EditText server, port;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_options);
		
		prefs = getSharedPreferences(BrmTronClient.PREFS_FILE, MODE_PRIVATE);
		
		server = (EditText) findViewById(R.id.edit_server);
		port = (EditText) findViewById(R.id.edit_port);
		Button confirmButton = (Button) findViewById(R.id.confirm);
		
		loadOptions();
		
		confirmButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				setResult(RESULT_OK);
				finish();
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		saveOptions();
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadOptions();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		saveOptions();
	}
	
	private void saveOptions() {
		Editor edit = prefs.edit();
		edit.putString(BrmTronClient.PREFS_SERVER_KEY, server.getText().toString());
		edit.putString(BrmTronClient.PREFS_PORT_KEY, port.getText().toString());
		edit.commit();
	}
	
	private void loadOptions() {
		server.setText(prefs.getString(BrmTronClient.PREFS_SERVER_KEY, ""));
		port.setText(prefs.getString(BrmTronClient.PREFS_PORT_KEY, ""));
	}

}
