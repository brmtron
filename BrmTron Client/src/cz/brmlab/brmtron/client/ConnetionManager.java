package cz.brmlab.brmtron.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;

public class ConnetionManager {
	private static Socket sock;
	private static DataOutputStream outStream;
	private static BufferedReader inStream;
	static final String LOG_TAG = "BrmTron ConnectionManager";
	
	public void ConnectionManager() {

	}
	
	public void connect(String host, int port) {
		try {
			sock = new Socket(host, port);
			outStream = new DataOutputStream(sock.getOutputStream());
			inStream = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		} catch(UnknownHostException e) {
			Log.e(LOG_TAG, "Unknown host");
		} catch(Exception e) {
			Log.e(LOG_TAG, e.toString());
		}
	}
	
	public void disconnect(){
		try {
			outStream.close();
			inStream.close();
			sock.close();
		} catch(Exception e) {
			Log.e(LOG_TAG, e.toString());
		}
	}
	
	public DataOutputStream getOutputStream() {
		return outStream;
	}
	
	public BufferedReader getInputReader() {
		return inStream;
	}
}
