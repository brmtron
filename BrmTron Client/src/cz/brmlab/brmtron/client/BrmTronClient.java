package cz.brmlab.brmtron.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class BrmTronClient extends Activity {
    /** Called when the activity is first created. */
	
	public static final String PREFS_FILE = "BrmTronClient";
	public static final String PREFS_SERVER_KEY = "server";
	public static final String PREFS_PORT_KEY = "port";
	
	private static final int OPTIONS_ID = Menu.FIRST;
	
	private static final int ACTIVITY_OPTIONS = 0;
    
	SharedPreferences prefs;
	
	LocationManager locationManager;
	TextView port, server;
	
	Socket sock;
	DataOutputStream out;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        prefs = getSharedPreferences(PREFS_FILE, MODE_PRIVATE);
        
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location current = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        
        TextView tv1 = (TextView) findViewById(R.id.tv1);
		TextView tv2 = (TextView) findViewById(R.id.tv2);
		server = (TextView) findViewById(R.id.tv_server);
		port = (TextView) findViewById(R.id.tv_port);
		if(current != null) {
			tv1.setText(Double.toString(current.getLongitude()));
			tv2.setText(Double.toString(current.getLatitude()));
		}
		server.setText(prefs.getString(PREFS_SERVER_KEY, ""));
		port.setText(prefs.getString(PREFS_PORT_KEY, ""));
		
		try {
			Log.i("brm", "TCP start");
			sock = new Socket(prefs.getString(PREFS_SERVER_KEY, ""), Integer.parseInt(prefs.getString(PREFS_PORT_KEY, "")));
			Log.i("brm", "TCP OK");
			out = new DataOutputStream(sock.getOutputStream());
			out.writeBytes("BrmTron Client\n");
		} catch(Exception e) {
			Log.e("brm", e.toString());
		}
		
		
        LocationListener locationListener = new LocationListener() {
        	
        	TextView tv1 = (TextView) findViewById(R.id.tv1);
			TextView tv2 = (TextView) findViewById(R.id.tv2);
			
			public void onStatusChanged(String provider, int status, Bundle extras) {
				
			}
			
			public void onProviderEnabled(String provider) {
				Location current = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				tv1.setText(Double.toString(current.getLongitude()));
				tv2.setText(Double.toString(current.getLatitude()));
			}
			
			public void onProviderDisabled(String provider) {
				tv1.setText(R.string.provider_disabled);
				tv2.setText("");
				
			}
			
			public void onLocationChanged(Location location) {
				
				String longitude = Double.toString(location.getLongitude());
				String latitude = Double.toString(location.getLatitude());
				
				tv1.setText(longitude);
				tv2.setText(latitude);
				try {
					if(sock == null) {
						try {
							Log.i("brm", "TCP start");
							sock = new Socket(prefs.getString(PREFS_SERVER_KEY, ""), Integer.parseInt(prefs.getString(PREFS_PORT_KEY, "")));
							Log.i("brm", "TCP OK");
							out = new DataOutputStream(sock.getOutputStream());
							out.writeBytes("BrmTron Client\n");
						} catch(Exception e) {
							Log.e("brm", e.toString());
						}
					}
					out.writeBytes(longitude+" "+latitude+"\n");
				} catch(Exception e){
					Log.e("brm", e.toString());
				}
				
			}
		};
		
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, OPTIONS_ID, 0, R.string.menu_options);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch(item.getItemId()){
		case OPTIONS_ID:
			showOptions();
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	private void showOptions(){
		Intent i = new Intent(this, EditOptions.class);
		startActivityForResult(i, ACTIVITY_OPTIONS);
	}

	@Override
	protected void onResume() {
		super.onResume();
		server.setText(prefs.getString(PREFS_SERVER_KEY, ""));
		port.setText(prefs.getString(PREFS_PORT_KEY, ""));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			out.close();
			sock.close();
		} catch(IOException e) {
			Log.e("brm", e.toString());
		} catch(Exception e) {
			Log.e("brm", e.toString());
		}
	}
	
	
	
	
	
	
}